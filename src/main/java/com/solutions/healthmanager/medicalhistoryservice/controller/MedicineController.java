package com.solutions.healthmanager.medicalhistoryservice.controller;

import com.solutions.healthmanager.medicalhistoryservice.service.logic.MedicineService;
import com.solutions.healthmanager.medicalhistoryservice.service.model.MedicineDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(path = "/medhistory/medicines")
@RequiredArgsConstructor
public class MedicineController {
    private final MedicineService medicineService;

    @GetMapping(value = "/{medicine_id}")
    public MedicineDto getVisitById(@PathVariable(name = "medicine_id") UUID medicineId){
        return medicineService.getMedicineById(medicineId);
    }
    @PostMapping
    public MedicineDto create(@RequestBody MedicineDto medicineDto){
        return medicineService.create(medicineDto);
    }
    @PutMapping
    public MedicineDto update(@RequestBody MedicineDto medicineDto){
        return medicineService.update(medicineDto);
    }
    @DeleteMapping("/{medicine_id}")
    public void deleteById(@PathVariable(name = "medicine_id") UUID medicineId){
        medicineService.delete(medicineId);
    }
}
