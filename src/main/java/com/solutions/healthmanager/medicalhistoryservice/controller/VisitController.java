package com.solutions.healthmanager.medicalhistoryservice.controller;

import com.solutions.healthmanager.medicalhistoryservice.service.logic.VisitService;
import com.solutions.healthmanager.medicalhistoryservice.service.model.VisitDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(path = "/medhistory/visits")
@RequiredArgsConstructor
public class VisitController {
    private final VisitService visitService;

    @GetMapping(value = "/{visit_id}")
    public VisitDto getVisitById(@PathVariable(name = "visit_id") UUID visitId){
        return visitService.getVisitById(visitId);
    }
    @PostMapping
    public VisitDto create(@RequestBody VisitDto visitDto){
        return visitService.create(visitDto);
    }
    @PutMapping
    public VisitDto update(@RequestBody VisitDto visitDto){
        return visitService.update(visitDto);
    }
    @DeleteMapping("/{visit_id}")
    public void deleteById(@PathVariable(name = "visit_id") UUID visitId){
        visitService.delete(visitId);
    }
}
