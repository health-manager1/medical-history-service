package com.solutions.healthmanager.medicalhistoryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedicalHistoryServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MedicalHistoryServiceApplication.class, args);
    }

}
