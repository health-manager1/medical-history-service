package com.solutions.healthmanager.medicalhistoryservice.service.logic;

import com.solutions.healthmanager.medicalhistoryservice.service.model.VisitDto;
import org.springframework.stereotype.Service;

import java.util.UUID;


public interface VisitService {
    VisitDto getVisitById(UUID visitId);
    VisitDto create(VisitDto visitDto);
    VisitDto update(VisitDto visitDto);
    void delete(UUID visitId);
}
