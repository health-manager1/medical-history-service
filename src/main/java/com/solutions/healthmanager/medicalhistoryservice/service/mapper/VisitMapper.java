package com.solutions.healthmanager.medicalhistoryservice.service.mapper;

import com.solutions.healthmanager.medicalhistoryservice.persistence.entity.Visit;
import com.solutions.healthmanager.medicalhistoryservice.service.model.VisitDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class VisitMapper {
    public abstract VisitDto fromEntity(Visit visit);
    public abstract Visit toEntity(VisitDto visitDto);
}
