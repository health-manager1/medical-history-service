package com.solutions.healthmanager.medicalhistoryservice.service.model;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;
@Getter
@Setter
public class MedicineDto {
    private UUID id;
    private String name;
    private String description;
}
