package com.solutions.healthmanager.medicalhistoryservice.service.logic;

import com.solutions.healthmanager.medicalhistoryservice.persistence.entity.Visit;
import com.solutions.healthmanager.medicalhistoryservice.persistence.repository.VisitRepository;
import com.solutions.healthmanager.medicalhistoryservice.service.mapper.VisitMapper;
import com.solutions.healthmanager.medicalhistoryservice.service.model.VisitDto;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
@Transactional
@AllArgsConstructor
@Service
public class VisitServiceImpl implements VisitService{
    private final VisitRepository visitRepository;
    private final VisitMapper visitMapper;
    @Override
    public VisitDto getVisitById(UUID visitId) {
        return visitMapper.fromEntity(visitRepository.getReferenceById(visitId));
    }

    @Override
    public VisitDto create(VisitDto visitDto) {
        return Optional.of(visitDto)
                .map(visitMapper::toEntity)
                .map(visitRepository::save)
                .map(visitMapper::fromEntity)
                .orElse(null);

    }

    @Override
    public VisitDto update(VisitDto visitDto) {

        return  Optional.of(visitDto)
                .map(visitMapper::toEntity)
                .map(visitRepository::save)
                .map(visitMapper::fromEntity)
                .orElse(null);

    }

    @Override
    public void delete(UUID visitId) {
        visitRepository.deleteById(visitId);
    }
}
