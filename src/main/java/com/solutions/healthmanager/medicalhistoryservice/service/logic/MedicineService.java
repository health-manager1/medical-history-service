package com.solutions.healthmanager.medicalhistoryservice.service.logic;

import com.solutions.healthmanager.medicalhistoryservice.service.model.MedicineDto;
import org.springframework.stereotype.Service;

import java.util.UUID;


public interface MedicineService {
   MedicineDto getMedicineById(UUID medicineId);
    MedicineDto create(MedicineDto medicineDto);
    MedicineDto update(MedicineDto medicineDto);
    void delete(UUID medicineId);
}
