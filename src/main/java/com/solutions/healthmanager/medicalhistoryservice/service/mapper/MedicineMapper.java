package com.solutions.healthmanager.medicalhistoryservice.service.mapper;

import com.solutions.healthmanager.medicalhistoryservice.persistence.entity.Medicine;
import com.solutions.healthmanager.medicalhistoryservice.service.model.MedicineDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class MedicineMapper {
    public abstract MedicineDto fromEntity(Medicine medicine);
    public abstract Medicine toEntity(MedicineDto medicineDto);
}
