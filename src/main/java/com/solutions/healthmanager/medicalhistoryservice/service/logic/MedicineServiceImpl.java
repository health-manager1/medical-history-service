package com.solutions.healthmanager.medicalhistoryservice.service.logic;

import com.solutions.healthmanager.medicalhistoryservice.persistence.repository.MedicineRepository;
import com.solutions.healthmanager.medicalhistoryservice.service.mapper.MedicineMapper;
import com.solutions.healthmanager.medicalhistoryservice.service.model.MedicineDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
@Service
@AllArgsConstructor
public class MedicineServiceImpl implements MedicineService{
    private final MedicineRepository medicineRepository;
    private final MedicineMapper medicineMapper;
    @Override
    public MedicineDto getMedicineById(UUID medicineId) {
        return medicineMapper.fromEntity(medicineRepository.getReferenceById(medicineId));
    }

    @Override
    public MedicineDto create(MedicineDto medicineDto) {
        return Optional.of(medicineDto)
                .map(medicineMapper::toEntity)
                .map(medicineRepository::save)
                .map(medicineMapper::fromEntity)
                .orElse(null);
    }

    @Override
    public MedicineDto update(MedicineDto medicineDto) {
        return Optional.of(medicineDto)
                .map(medicineMapper::toEntity)
                .map(medicineRepository::save)
                .map(medicineMapper::fromEntity)
                .orElse(null);
    }

    @Override
    public void delete(UUID medicineId) {
        medicineRepository.deleteById(medicineId);

    }
}
