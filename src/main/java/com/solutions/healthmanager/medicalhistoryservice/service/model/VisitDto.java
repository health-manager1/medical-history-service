package com.solutions.healthmanager.medicalhistoryservice.service.model;

import com.solutions.healthmanager.medicalhistoryservice.persistence.entity.Medicine;
import jakarta.persistence.Column;
import jakarta.persistence.ManyToMany;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
@Getter
@Setter
public class VisitDto {
    private UUID id;
    private LocalDate date;
    private String diseaseName;
    private String complaints;
    private String doctorsName;
    private List<Medicine> medicines;
}
