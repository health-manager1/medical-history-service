package com.solutions.healthmanager.medicalhistoryservice.persistence.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Table(name = "visits")
@Entity
@Getter
@Setter
public class Visit {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;
    private LocalDate date;
    @Column(name = "disease_name")
    private String diseaseName;
    private String complaints;
    @Column(name = "doctors_name")
    private String doctorsName;
    @Column(name = "user_id")
    private UUID userId;
    @ManyToMany
    private List<Medicine> medicines;
}
