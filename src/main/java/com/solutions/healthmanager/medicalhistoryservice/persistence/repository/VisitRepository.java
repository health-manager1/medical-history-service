package com.solutions.healthmanager.medicalhistoryservice.persistence.repository;

import com.solutions.healthmanager.medicalhistoryservice.persistence.entity.Visit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface VisitRepository extends JpaRepository<Visit, UUID> {
}
