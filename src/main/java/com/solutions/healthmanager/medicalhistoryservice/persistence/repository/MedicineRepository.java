package com.solutions.healthmanager.medicalhistoryservice.persistence.repository;

import com.solutions.healthmanager.medicalhistoryservice.persistence.entity.Medicine;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface MedicineRepository extends JpaRepository<Medicine, UUID> {
}
